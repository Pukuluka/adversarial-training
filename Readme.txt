Автор: Дмитрий Пришлецов
эл. почта: i@pukuluka.ru
тема: Атака на систему классификации изображений: мимикрия

Используется:
Stanford Car Dataset (https://www.kaggle.com/datasets/jutrera/stanford-car-dataset-by-classes-folder)
Pytorch car classifier от DEEPBEAR (https://www.kaggle.com/code/deepbear/pytorch-car-classifier-90-accuracy)

0x_Model.pt - бинарные файлы с результатами обучения для каждого из этапов работы

Директория IN - для файлов изображений автомобилей